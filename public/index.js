(function() {
    // Create an instance of Angular app
    var CartApp = angular.module("CartApp",[]);

    var CartCtrl = function ($http, $log) {
        var cartCtrl = this;   // to preseve the ownership


        cartCtrl.to = "";
        cartCtrl.from = "";
        cartCtrl.note = "";
        cartCtrl.flower = "";
        cartCtrl.colors = [];
        cartCtrl.quantity = 0;
        cartCtrl.basket = {};
        cartCtrl.pink = false;
        cartCtrl.red = false;
        cartCtrl.blue = false;
        cartCtrl.white = false;

        cartCtrl.addToBasket = function(){
            // var myPurchase = this;
            // console.log(myCart.item);
            // console.log(myCart.quantity);
            // var idx = cartCtrl.basket.findIndex(function(elem){
            //     return (elem.item == cartCtrl.item);

            // });

            // console.log(idx);
            if ( cartCtrl.red)
                cartCtrl.colors.push("red");
            
            if ( cartCtrl.pink)
                cartCtrl.colors.push("pink"); 

            if ( cartCtrl.blue)
                cartCtrl.colors.push("blue"); 

            if ( cartCtrl.white)
                cartCtrl.colors.push("white"); 


        //    // if (-1 == idx)
        //         cartCtrl.basket.push ({
        //             to: cartCtrl.to,
        //             from: cartCtrl.from,
        //             note: cartCtrl.note,
        //             flower: cartCtrl.flower,
        //             quantity: cartCtrl.quantity,
        //             colors: cartCtrl.colors
        //         });
        //     // else
            //     cartCtrl.basket[idx].quantity += cartCtrl.quantity;

            // Reset the model    
            // cartCtrl.item = "";
            // cartCtrl.quantity = 0;
            // basket = {  to: cartCtrl.to,
            //         from: cartCtrl.from,
            //         note: cartCtrl.note,
            //         flower: cartCtrl.flower,
            //         quantity: cartCtrl.quantity,
            //         colors: cartCtrl.colors };


            // $http.get ("/order", {params : basket});
            $http.get("/order", { params: {
                    to: cartCtrl.to,
                    from: cartCtrl.from,
                    note: cartCtrl.note,
                    flower: cartCtrl.flower,
                    quantity: cartCtrl.quantity,
                    colors: cartCtrl.colors
            }}  );



            console.log(JSON.stringify(cartCtrl.basket));

            
        }



    }


    CartApp.controller("CartCtrl",[ "$http", "$log",CartCtrl ]);




})();

