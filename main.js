// load libraries
var express = require("express");
var path = require("path");

// instantiate express
var app = express();
var basket = [];
// set port
app.set("port",parseInt(process.argv[2]) || process.env.APP_PORT || 3000 );

// routes
app.use(express.static(path.join(__dirname,"public")));

// routes for Angular
app.use("/libs",express.static(path.join(__dirname,"bower_components")));

app.get("/order", function(req, resp){
    console.log("/order");
    console.log([req.query.to,req.query.from,req.query.note]);
    basket.push({
        to : req.query.to,
        from : req.query.from,
        note : req.query.note
    
    });
    console.log(basket);
    resp.status(202);
    resp.end();

});


app.listen(app.get("port"), function(){
    console.log("Application started at %s on port %d", new Date(),app.get("port"));


});